from django.shortcuts import render, get_object_or_404
from django.http import Http404
from django.http import HttpResponse, HttpRequest, HttpResponseServerError, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt # else POST error 403 "CSRF cookie not set"
from layered_image.models import LayeredFile, DirPath, Layer
import logging, json
from django.core import serializers

logger = logging.getLogger("layered_image")
error  = logger.error
trace  = logger.info

# For returning json see: http://rayed.com/wordpress/?p=1508

def index(request):
    context = None
    return render(request, 'layered_image/index.html', context)

@csrf_exempt
def layeredfile(request):
    rval = {'result': 'error'}
    if request.method == 'POST':
        try:
            bodydata = request.body
            json_data = json.loads(bodydata)
        except Exception as ex:
            HttpResponseServerError('Error reading json: %s' % str(ex))
            
        name = json_data['name']    
        json_data.pop('id') # id will be bogus
        try:
            LayeredFile.objects.get(name=name)
            HttpResponseBadRequest("name exists in LayeredFiles")
        except LayeredFile.DoesNotExist:
            obj = LayeredFile.objects.create(**json_data)
            rval = json.dumps(obj.to_dict())
            trace('Created: LayeredFile(id=%s, sha1=%s, width=%s, height=%s, name=%s)' % 
                  (obj.id, obj.sha1, obj.width, obj.height, obj.name))       
    return HttpResponse(rval, mimetype='application/json')

@csrf_exempt
def layer(request):
    rval = {'result': 'error'}
    if request.method == 'POST':
        try:
            bodydata = request.body
            json_data = json.loads(bodydata)
        except Exception as ex:
            HttpResponseServerError('Error reading json: %s' % str(ex))
            
        index = json_data['index']  
        fileid = json_data['file']
        try:
            ffile = LayeredFile.objects.get(id=fileid)
            Layer.objects.get(index=index, file=fileid)
            HttpResponseBadRequest("name exists in Layers")
        except LayeredFile.DoesNotExist:
            pass
        except Layer.DoesNotExist:
            json_data['file'] = ffile
            obj = Layer.objects.create(**json_data)
            rval = json.dumps(obj.to_dict())
            trace("Created: Layer(id=%d, name='%s', index=%d, file=%d, (%d, %d, %d, %d)" % 
                  (obj.id, obj.name, obj.index, fileid, obj.x1, obj.y1, obj.x2, obj.y2))
      
    return HttpResponse(rval, mimetype='application/json')
    