from django.conf.urls import patterns, url
from layered_image import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^layeredfile/$', views.layeredfile, name='layeredfile'),
    url(r'^layer/$', views.layer, name='layer'),
)