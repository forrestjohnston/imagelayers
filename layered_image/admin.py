from django.contrib import admin
from layered_image.models import DirPath, LayeredFile, Group, Layer, LayerAccess

for mod in (DirPath, LayeredFile, Group, Layer, LayerAccess):
    admin.site.register(mod)