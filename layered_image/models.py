from django.db import models
from django.conf import settings
    
class DirPath(models.Model):
    dir = models.CharField(max_length=255)
    def to_dict(self):
        return dict( id   = self.id, 
                     dir  = settings.MEDIA_URL + self.dir )
    def __unicode__(self):
        return self.name

class LayeredFile(models.Model):
    #dir     = models.ForeignKey(DirPath)
    name    = models.CharField(max_length=200)
    sha1    = models.CharField(max_length=42)
    width   = models.IntegerField()
    height  = models.IntegerField()
    def to_dict(self):
        return dict( id   = self.id, 
                     name = self.name, 
                     sha1 = self.sha1, 
                     width = self.width, 
                     height = self.height )
    def __unicode__(self):
        return self.name
    
class Group(models.Model):
    name = models.CharField(max_length=80)
    layer = models.ForeignKey(LayeredFile)
    
class Layer(models.Model):
    file      = models.ForeignKey(LayeredFile)
    name      = models.CharField(max_length=80)
    index     = models.IntegerField()   # 0=backgroud, max_range=topmost
    opacity   = models.SmallIntegerField() # range(0, 255)
    visible   = models.BooleanField()
    x1 = models.IntegerField()
    y1 = models.IntegerField()
    x2 = models.IntegerField()
    y2 = models.IntegerField()
    def to_dict(self):
        return dict( id      = self.id, 
                     name    = self.name, 
                     index   = self.index, 
                     opacity = self.opacity, 
                     x1 = self.x1, 
                     y1 = self.y1,
                     x2 = self.x2, 
                     y2 = self.y2, )
    def __unicode__(self):
        return self.name

class LayerAccess(models.Model):
    layer       = models.ForeignKey(Layer)
    view_user   = models.BooleanField()
    edit_user   = models.BooleanField()
    view_pub    = models.BooleanField()
    edit_pub    = models.BooleanField()
    